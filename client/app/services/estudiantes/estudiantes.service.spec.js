'use strict';

describe('Service: estudiantes', function () {

  // load the service's module
  beforeEach(module('proyectoFullstackApp'));

  // instantiate service
  var estudiantes;
  beforeEach(inject(function (_estudiantes_) {
    estudiantes = _estudiantes_;
  }));

  it('should do something', function () {
    expect(!!estudiantes).toBe(true);
  });

});
