'use strict';

angular.module('proyectoFullstackApp')
    .controller('JavaDetailCtrl',
    function ($moment, $scope, $stateParams, Estudiante) {
        var vm = this;

        vm.isDateOpened = false;

        vm.isValidModel = isValidModel;
        vm.openDateSelector = openDateSelector;
        vm.saveChanges = saveChanges;

        activate();

        function activate() {
            vm.promise = Estudiante.get($stateParams.id);
            vm.promise.then(function (response) {
                vm.selected = response;
                vm.selected.fecha = new Date(response.fechaNacimiento);
            });
        }

        function isValidModel() {
            return vm.selected && vm.selected.nombre !== '' && vm.selected.apellido !== '';
        }

        function openDateSelector() {
            vm.isDateOpened = true;
        }

        function saveChanges() {
            //Format date
            vm.selected.fechaNacimiento = $moment(vm.selected.fecha).format('YYYY-MM-DDTHH:mm:ssZ');

            vm.promise = Estudiante.save($stateParams.id, vm.selected);
            vm.promise.then(function (response) {
                vm.selected = response;
                vm.selected.fecha = new Date(response.fechaNacimiento);
                //$scope.java.fillList();
                $scope.$emit('details-updated');
            });
        }

//        $scope.$watch('javaDetail.selected.nombre',
//            function(nuevo, anterior){
//                console.log('nuevo: ', nuevo);
//                console.log('anterior: ', anterior);
//            });

        // Observar cambios en todo el modelo
        $scope.$watch('javaDetail.selected',
            function (nuevo, anterior) {
                console.log('nuevo: ', nuevo);
                console.log('anterior: ', anterior);
            }, true);
    });
