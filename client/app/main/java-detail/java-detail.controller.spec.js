'use strict';

describe('Controller: JavaDetailCtrl', function () {

  // load the controller's module
  beforeEach(module('proyectoFullstackApp'));

  var JavaDetailCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    JavaDetailCtrl = $controller('JavaDetailCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
